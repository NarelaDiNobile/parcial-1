using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Temporizador : MonoBehaviour
{
    int countDownStartValue = 60;
    public Text timerUI;
    public float timeLeft = 3.0f;
    public Text startText;
    public GameObject Jugador;

    public GameObject Reiniciar;
    public Image GameOver;
    public Image Ganador;
    
 
    void Start()
    {
        Ganador.enabled = false;
        GameOver.enabled = false;
        Reiniciar.gameObject.SetActive(false);
        countDownTimer();
    }

    void countDownTimer()
    {
        if (countDownStartValue > 0)
        {
            TimeSpan spanTime = TimeSpan.FromSeconds(countDownStartValue);
            timerUI.text = " " + spanTime.Minutes + " : " + spanTime.Seconds;
            countDownStartValue--;
            Invoke("countDownTimer", 1.0f);
        }
        else
        {
            Ganador.gameObject.SetActive(true);
            GameOver.gameObject.SetActive(true);
            Reiniciar.gameObject.SetActive(true);
            GameOver.enabled = true;
            Ganador.enabled = false;
        }
    }
    
    void Update()
    {
    }
}



