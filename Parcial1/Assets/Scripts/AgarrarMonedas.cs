using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgarrarMonedas : MonoBehaviour
{
    void Start()
    {

    }

    void Update()
    {
    }

    private void OnTriggerStay(Collider other)
    {
        Debug.Log(other.tag);

        if (other.tag == "Objeto" && other.GetComponent<LogicalMonedas>().destruirAutomatico == true)
        {
            other.GetComponent<LogicalMonedas>().Efecto();
            Destroy(other.gameObject);
        }
    }  
}
