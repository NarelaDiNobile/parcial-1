using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using System;

public class ControlJugador : MonoBehaviour
{
    public float velocidadInicial = 3;
    public Text textoCantidadDeMonedas;
    public Image textoGanador;
  
    int Monedas = 0;

    Rigidbody miRigidbody;
    Vector3 posicionInical;
    bool aSalido;


    void Start()
    {
        miRigidbody = GetComponent<Rigidbody>();
        posicionInical = transform.position;
        textoGanador.enabled = false;
        aSalido = false;

    }

    void Update()
    {
        if (!aSalido)
        {
            float horizontal = Input.GetAxis("Horizontal");
            float Vertical = Input.GetAxis("Vertical");

            miRigidbody.AddForce(new Vector3(horizontal, 0, Vertical) * velocidadInicial);

            if (Input.GetKeyDown(KeyCode.R))
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            {
            }
        }

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Salida/Ganador"))
        {
            aSalido = true;
            textoGanador.enabled = true;
            miRigidbody.velocity = Vector3.zero;
            miRigidbody.angularVelocity = Vector3.zero;
        }

        else if (other.CompareTag("Enemigo"))
        {
            miRigidbody.MovePosition(posicionInical);
            miRigidbody.velocity = Vector3.zero;
            miRigidbody.angularVelocity = Vector3.zero;
            Monedas = 0;
            textoCantidadDeMonedas.text = "Cantidad de Monedas:  0";
        }

        else if (other.CompareTag("Moneda"))
        {
            other.gameObject.SetActive(false);
            Monedas = Monedas + 1;
            textoCantidadDeMonedas.text = "Cantidad de Monedas:" + Monedas;
        }
    }
}

