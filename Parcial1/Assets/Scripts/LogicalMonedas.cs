using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogicalMonedas : MonoBehaviour
{
    public bool destruirAutomatico;
    public ControlJugador controlJugador;

    public int tipo;

    //1 = crecer
    //2 = aumento de velocidad
    //3 = lento

    void Start()
    {
        controlJugador = GameObject.FindGameObjectWithTag("Player").GetComponent<ControlJugador>();
    }

    void Update()
    {
        
    }

    public void Efecto()
    {
        switch (tipo)
        {
            case 1:
                controlJugador.gameObject.transform.localScale = new Vector3(3, 3, 3);
                break;
            case 2:
                controlJugador.velocidadInicial += 2;
                break;
            case 3:
                controlJugador.velocidadInicial -= 2;
                break;
            case 4:
                controlJugador.gameObject.transform.localScale = new Vector3(1, 1, 1);
                break;



            default:
                Debug.Log("sin efecto");
                break;
        }
    }
}
